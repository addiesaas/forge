# addiesaas-forge

## AddieSaaS CLI tool to create new AddieSaaS Application as automatically as possible.

The tool does the following:
1. Creates the core application - using `git clone` followed by `composer install`.
2. Registers external composer repositories (optional).
3. Adds containers to the application using `composer require`.
3. Creates and populates `.env` file.   
4. Runs these php artisn commands
    1. `key:generate`
    2. `config:cache`
    3. `route:cache`
    4. `migrate`
    5. `passport:install`
    6. `storage:link`
    7. `db:seed`
    8. `npm install && npm run prod`


### USAGE:
    addiesaas-forge <OPTIONS> <COMMAND> ...

### COMMANDS:
This tool accepts a command as first parameter as outlined below:


    create

Create addiesaas.json for new application

    install

Install application

---

## STEPS

### 1. Create

1. Open a blank directory.
2. Run this command.
   
        addiesaas-forge create
3. A new file named `addiesaas.json` will be created. Edit this file in a text editor.
   * Add/Update/Delete containers (will be discussed later)
   * Change options (will be discussed later) 
4. Run this command
   
        addiesaas-install

---

## addiesaas.json

A freshly created `addiesaas.json` using `create` command looks like this.

    {
     "team": "addiesaas",
     "vcs": "git@bitbucket.org",
     "_app": {
       "repo": "git@bitbucket.org:addiesaas/application-core.git",
       "branch": "master",
       "tag": ""
     },
     "app": "git@bitbucket.org:addiesaas/application-core.git",
     "_repositories": {
       "name": "url",
       "name2": {
           "type": "vcs"
           "url": "url"
       },
     },
     "repositories": {
     },
     "containers": {
       "container-admin-backpack": "dev-master",
       "container-main": "dev-master",
       "container-utils-asset-proxy": "dev-master",
       "container-dev-swagger-ui": "dev-master",
       "container-pay-biller": "dev-master",
       "container-pay-biller-authorizenet": "dev-master",
       "container-shop-products": "dev-master",
       "container-shop-orders": "dev-master",
       "container-builders-embeddedforms": "dev-master",
       "container-builders-grapesjs": "dev-master",
       "container-builders-surveryjs": "dev-master",
       "container-marketing-coupons": "dev-master",
       "container-marketing-gift-cards": "dev-master",
       "container-marketing-promotion": "dev-master",
       "container-marketing-segments": "dev-master",
       "container-social-channel": "dev-master"
     },
     "options": {
       "no-pre-install": false,
       "keygen": true,
       "passport": true,
       "config-cache": true,
       "route-cache": true,
       "storage-link": true,
       "migrate": true,
       "seed": true,
       "npm": true,    
       "env-create": true,
       "env": {
         "LOG_CHANNEL": "daily",
         "DB_CONNECTION": "mysql",
         "DB_HOST": "localhost",
         "DB_PORT": 3306,
         "DB_DATABASE": "rocketeffect",
         "DB_USERNAME": "root",
         "DB_PASSWORD": "root",
         "MAIL_ENABLED": "false",
         "MAIL_DRIVER": "log",
         "APP_NAME": "Rocket Effect",
         "APP_SHORT_NAME": "RE",
         "APP_NAME_SUBTITLE": "Rocket Effect Admin",
         "APP_URL": "http://rocketeffect.local",
         "MAIL_FROM_ADDRESS": "test@test.test",
         "MAIL_FROM_NAME": "Rocket Effect",
         "MAIL_TO_SUPPORT_ADDRESS": "test@test.com",
         "MAIL_TO_SUPPORT_NAME": "Rocket Effect Support"
       }
     }
    }

### Properties of addiesaas.json
> All properties prefixed with `_` are examples. 

#### team
Team can be any name. You can keep it as it is.

#### vcs (deprecated)

Main source of the repositories. You can also keep it as it is.

> This property is deprecated.


#### app

The core application repository - `git@bitbucket.org:addiesaas/application-core.git`

The value can be: 
- A string which is the URL of the core application repository. By default,  the `master` branch of the repository will be checked out. 
- An object with the following properties
   - `repo`: The URL of the core application repository, e.g., `"git@bitbucket.org:addiesaas/application-core.git"`
   - `branch`: Name of the branch e.g., `"develop"`
   - `tag`: Name of a tag/version e.g., "v1.5.1" 

#### repositories (optional)

Registers composer packages from external sources.

The value is an object (key-value pairs) where:
* the `key` (property name) is a unique name of the package.
* The `value` (value for each property) can be:
  * a string representing the URL of the package, usually a git repository URL.
  * OR, an object with two properties: 
    * `type`: Usually the value will be `"vcs"`.
    * `url`: The URL of the package, usually a git repository URL. 

> When the `value` is a string, the `type` is assumed as `vcs`.

Example: 

    "repositories": {
        "modified-ffmpeg": "https://github.com/modifier/laravel-ffmpeg-php72",
        "another-example": {
            "type": "vcs",
            "url": "https://github.com/auther/package"
        }
     },

#### containers

Adds addiesaas container to the application.

The value is an object (key-value pairs) where:
* the `key` represents the name of the container. e.g., "container-shop-products"  
  Container name have a specific format. 
  * It always starts with `"container-"`, 
  * followed by section name, e.g., `"shop"` and then a `-` (dash)
  * followed by the name of the container, e.g., `"products"`
    
    *Exception: The name `"conatiner-main"` does not have a section name.*     
    
* the value represents the version/tag e.g., 
  - `"dev-master"`
  - `"dev-develop"`
  - `"1.2.1"` 

#### options
Additional options that controls the installation  of the application.

##### no-pre-install (default: `false`)

If set to `true`, it asks the Tool not to install the core application.
The Tool will assume that the core application is already installed.  

> The name sounds weird and confusing. We may change it later. 

##### env-create (default: `true`)

Asks the Tool to create `.env` file. If set to `false`, the Tool will assume that an `.env` file exists.

> When the Tool creates a new `.env` file, it copies from `.env.example` file.

##### keygen (default: `true`)

If set to `true`, it asks the tool to generate application key in `.env` file. 
Runs the following command:

> php artisan key:generate

##### passport (default: `true`)
If set to `true`, it asks the tool to install Passport keys.

Runs the following command:

> php artisan passport:install

##### config-cache (default: `true`)
If set to `true`, it asks the tool to  re-cache config.
Runs the following command:

> php artisan config:cache

##### route-cache (default: `true`)
If set to `true`, it asks the tool to re-cache routes.
Runs the following command:

> php artisan route:cache

##### storage-link (default: `true`)
If set to `true`, it asks the tool to link storage to public.
Runs the following command:

> php artisan storage:link

##### migrate (default: `true`)
If set to `true`, it asks the tool to run data migration..
Runs the following command:

> php artisan migrate

##### seed (default: `true`) 
If set to `true`, it asks the tool to  seed data to database.
Runs the following command:

> php artisan db:seed

##### npm (default: `true`)
If set to `true`, it asks the tool to install npm packages and build assets.
Runs the following command:

> npm install && npm run prod

##### env

The value accepts an object. The object represents env variables.
The `key` (property name) represents the name of the env variable.
The `value` represents the value of the env variable.

Example: 

    {
        "DB_HOST": "localhost",
        "DB_PORT": 3306,
        "DB_DATABASE": "rocketeffect",
        "DB_USERNAME": "root",
        "DB_PASSWORD": "root",
    }

---





    
       
       


