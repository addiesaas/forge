<?php

namespace Addiesaas\Core\Support\Forge;

/**
 * Before adding containers list in addiesaas.json make sure to set SSH keys to the container repositories
 *
 * @requires GnuWin CoreUtilities Packages for Windows http://gnuwin32.sourceforge.net/packages.html
 *           http://gnuwin32.sourceforge.net/packages/coreutils.htm
 */
class Install
{
    public function myexec($command, $message = '')
    {
        echo $message . PHP_EOL;
        system($command . " 2>&1");
    }

    public function run($config = [])
    {
        $jsonFileName = $config['file'] ?? 'addiesaas.json';
        echo "Reading configuration ./" . $jsonFileName . PHP_EOL;

        $data = json_decode(@file_get_contents('./'.$jsonFileName) ?? "{}", true) ?? [];

        //        return false;
        if (!empty($data)) {
            /** @var $app */
            /** @var $containers */
            /** @var $vcs */
            /** @var $team */
            /** @var $options */
            /** @var $repositories */
            extract($data);

            $defaultOptions = [
                "no-pre-install" => false,
                "env-create" => false,
                "env" => [],
                "keygen" => false,
                "migrate" => false,
                "seed" => false,
                "passport" => false,
                "config-cache" => false,
                "route-cache" => false,
                "storage-link" => false,
                "npm" => false,
            ];
            $options = array_merge($defaultOptions, $options ?? []);

            if (empty($options['no-pre-install'])) {
                if (is_string($app)) {
                    $app = [
                        "repo" => $app,
                        "branch" => 'master',
                    ];
                }

                $this->myexec("git clone --single-branch --branch {$app['branch']} {$app['repo']} repo",
                    "Retrieving core from from git repo {$app['repo']}:{$app['branch']}");
                if (!empty($app['tag'])) {
                    $this->myexec("git checkout tags/{$app['tag']}", "Checkout Tag");
                }

                $this->myexec("mv repo/.* ./", "Copy hidden files");
                $this->myexec("mv repo/* ./", "Copy application files");
                $this->myexec("rm -rf repo");

                $this->myexec("composer install", "composer install");
            }

            //
            if (!empty($repositories) && is_array($repositories)) {
                foreach ($repositories as $name => $repo) {
                    $url = is_string($repo) ? $repo : $repo['url'];
                    $type = $repo['type'] ?? 'vcs';
                    $this->myexec("composer config repositories.{$name} {$type} {$url}",
                        "Registering external repository from {$url}");
                }
            }

            $requires = [];
            foreach ($containers as $name => $version) {
                $requires[] = "{$team}/{$name}:{$version}";
            }

            if (!empty($requires)) {
                $require = implode(' ', $requires);
                $this->myexec("composer require {$require}", "Installing containers");
            }

            $commandStore = [
                "keygen" => "php artisan key:generate",
                "config-cache" => "php artisan config:cache",
                "route-cache" => "php artisan route:cache",
                "migrate" => "php artisan migrate",
                "passport" => "php artisan passport:install",
                "storage-link" => "php artisan storage:link",
                "seed" => "php artisan db:seed",
                "npm" => "npm install && npm run prod",
            ];

            if ($options['env-create']) {
                if (!file_exists('.env')) {
                    echo "Create .env file" . PHP_EOL;
                    copy('.env.example', '.env');
                }
            }
            if (!empty($options['env'])) {
                foreach ($options['env'] as $key => $val) {
                    $this->myexec("php artisan env:set $key \"$val\"", "Add ENV: $key  = \"$val\"");
                }
            }

            foreach ($commandStore as $key => $command) {
                if ($options[$key]) {
                    $this->myexec($command, "Run command: $command");
                }
            }

            $this->myexec('git checkout -- composer.json', "Reset composer.json");
            $this->myexec('git checkout -- composer.lock', "Reset composer.json");

        } else {
            echo $jsonFileName . " not found or configured incorrectly." . PHP_EOL;
        }
    }

    public function create($config = [])
    {
        $jsonFileName = $config['file'] ?? 'addiesaas.json';
        if (!(file_exists('./'. $jsonFileName))) {
            echo 'Creating '. $jsonFileName . PHP_EOL;
            $json = '{
  "team": "addiesaas",
  "vcs": "git@bitbucket.org",
  "_app": {
    "repo": "git@bitbucket.org:addiesaas/application-core.git",
    "branch": "master",
    "tag": ""
  },
  "app": "git@bitbucket.org:addiesaas/application-core.git",
  "_repositories": {
    "name": "url",
    "name2": {
        "type": "vcs",
        "url": "url"
    }
  },
  "repositories": {
  },
  "containers": {
    "container-admin-backpack": "dev-master",
    "container-main": "dev-master",
    "container-utils-asset-proxy": "dev-master",
    "container-dev-swagger-ui": "dev-master",
    "container-pay-biller": "dev-master",
    "container-pay-biller-authorizenet": "dev-master",
    "container-shop-products": "dev-master",
    "container-shop-orders": "dev-master",
    "container-builders-embeddedforms": "dev-master",
    "container-builders-grapesjs": "dev-master",
    "container-builders-surveryjs": "dev-master",
    "container-marketing-coupons": "dev-master",
    "container-marketing-gift-cards": "dev-master",
    "container-marketing-promotion": "dev-master",
    "container-marketing-segments": "dev-master",
    "container-social-channel": "dev-master"
  },
  "options": {
    "no-pre-install": false,
    "keygen": true,
    "passport": true,
    "config-cache": true,
    "route-cache": true,
    "storage-link": true,
    "migrate": true,
    "seed": true,
    "npm": true,    
    "env-create": true,
    "env": {
      "LOG_CHANNEL": "stack",
      "DB_CONNECTION": "mysql",
      "DB_HOST": "localhost",
      "DB_PORT": 3306,
      "DB_DATABASE": "rocketeffect",
      "DB_USERNAME": "root",
      "DB_PASSWORD": "root",
      "MAIL_ENABLED": "false",
      "MAIL_DRIVER": "log",
      "APP_NAME": "Rocket Effect",
      "APP_SHORT_NAME": "RE",
      "APP_NAME_SUBTITLE": "Rocket Effect Admin",
      "APP_URL": "http://rocketeffect.local",
      "MAIL_FROM_ADDRESS": "test@test.test",
      "MAIL_FROM_NAME": "Rocket Effect",
      "MAIL_TO_SUPPORT_ADDRESS": "test@test.com",
      "MAIL_TO_SUPPORT_NAME": "Rocket Effect Support"
    }
  }
}
';

            file_put_contents('./' . $jsonFileName, $json);
            echo $jsonFileName . ' created.' . PHP_EOL;

        } else {
            echo $jsonFileName . ' already exists!' . PHP_EOL;
        }
    }
}



