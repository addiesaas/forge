<?php

namespace Addiesaas\Core\Support\Forge;

use SudiptoChoudhury\Support\Abstracts\AbstractCli;
use splitbrain\phpcli\Options;


class Cli extends AbstractCli
{

    public static $rootPath = __DIR__;
    protected $versionName = 'version 1.4.1.1';
    protected $welcome = 'AddieSaaS CLI tool to create new AddieSaaS Application as automatically as possible. 

For details read: https://bitbucket.org/addiesaas/forge/src/master/README.md

';

    protected $config = [];
    protected $commandOptions = [];


    protected $log = null;
    protected $env = 'dev';
    protected $simulate = false;
    protected $quiet = false;
    protected $skipDefaults = false;
    protected $logdefault = 'debug';

    protected $moreCommands = [
        'install' => [
            'help' => 'Install application',
        ],
        'create' => [
            'help' => 'Create addiesaas.json for new application',
        ],
    ];

    protected function callApi($command, Options $options)
    {

    }

    public function install($options)
    {
        $installer = new Install();
        $installer->run($options);
    }

    public function create($options)
    {
        $installer = new Install();
        $installer->create($options);
    }
}